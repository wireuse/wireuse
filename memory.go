package main

type LastPeerUsage struct {
	Peer                 string
	Upload, Download, TS int64
}

type LastPeersUsage map[string]LastPeerUsage
