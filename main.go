package main

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
	"text/template"
	"time"

	"github.com/pressly/goose/v3"
	"github.com/rs/zerolog"
	"github.com/urfave/cli/v2"
	"github.com/xeptore/wireuse/pkg/funcutils"
	"golang.zx2c4.com/wireguard/wgctrl"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
	_ "modernc.org/sqlite"

	"gitlab.com/wireuse/wireuse/db/migration"
)

var (
	AppName        = ""
	AppVersion     = ""
	AppCompileTime = ""
)

const (
	FlagWGUpMarkerFilePath = "up-file"
	FlagDBPath             = "db"
)

var (
	log zerolog.Logger
	mem = make(LastPeersUsage)
)

func main() {
	log = zerolog.New(
		zerolog.NewConsoleWriter(func(w *zerolog.ConsoleWriter) {
			w.Out = os.Stderr
			w.TimeFormat = time.RFC3339
			w.NoColor = true
		}),
	).With().Timestamp().Logger().Level(zerolog.TraceLevel)

	compileTime, err := time.Parse(time.RFC3339, AppCompileTime)
	if nil != err {
		log.Fatal().Err(err).Msg("")
	}

	app := &cli.App{
		Name:     AppName,
		Version:  AppVersion,
		Compiled: compileTime,
		Suggest:  true,
		Usage:    "WireGuard Peers Usage Collector",
		Action:   run,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     FlagWGUpMarkerFilePath,
				Aliases:  []string{"u"},
				Usage:    "Path to file that is touched by WireGuard device in PostUp hook named in %i.up format, where %i is the name of the WireGuard device",
				Required: true,
			},
			&cli.StringFlag{
				Name:     FlagDBPath,
				Aliases:  []string{"d"},
				Usage:    "Database File Path",
				Required: true,
			},
		},
	}
	if err := app.Run(os.Args); err != nil {
		if !errors.Is(err, context.Canceled) {
			log.Fatal().Err(err).Msg("")
		}
	}
}

func run(cliCtx *cli.Context) error {
	dbFilePath, wgUpMarkerFilePath := cliCtx.String(FlagDBPath), cliCtx.String(FlagWGUpMarkerFilePath)

	ctx, cancel := signal.NotifyContext(cliCtx.Context, os.Interrupt, syscall.SIGTERM)
	defer cancel()

	db, err := sql.Open("sqlite", dbFilePath)
	if nil != err {
		return fmt.Errorf("unable to open database: %v", err)
	}
	defer func() {
		log.Info().Msg("closing database connection")
		if err := db.Close(); nil != err {
			log.Error().Err(err).Msg("failed to close database connection")
		}
	}()
	if err := db.PingContext(ctx); nil != err {
		return fmt.Errorf("unable to ping database connection: %v", err)
	}
	if err := execPragmas(ctx, db); nil != err {
		return fmt.Errorf("unable to execute database pragmas: %v", err)
	}
	log.Info().Msg("executed database pragmas")

	goose.SetLogger(goose.NopLogger())
	goose.SetTableName("migrations")
	goose.SetBaseFS(migration.FS)
	if err := goose.SetDialect("sqlite"); nil != err {
		return fmt.Errorf("unable to set goose dialect to sqlite: %v", err)
	}
	if err := goose.Up(db, "scripts"); nil != err {
		return fmt.Errorf("unable to execute goose migrations: %v", err)
	}
	log.Info().Msg("executed database migrations")

	wg, err := wgctrl.New()
	if nil != err {
		return fmt.Errorf("unable to instantiate wireguard controller: %v", err)
	}
	log.Info().Msg("initialized wg controller instance")

	var tickDur time.Duration = time.Second * 10
	log.Info().Dur("duration ms", tickDur).Msg("using tick duration")
	ticker := time.NewTicker(tickDur)

	insertQuery := newInsertQueryTemplate()

	for {
		select {
		case <-ctx.Done():
			log.Info().Msg("root context cancelled")
			ticker.Stop()
			return ctx.Err()
		case tick := <-ticker.C:
			log := log.With().Time("tick", tick).Logger()
			tickTime := tick.Unix()

			f, err := os.Stat(wgUpMarkerFilePath)
			if nil != err {
				if !errors.Is(err, os.ErrNotExist) {
					return fmt.Errorf("unable to stat wg up marker file: %v", err)
				}
				// It's because either the WireGuard device is down, which means we have nothing to do,
				// or it hasn't integrated the up file marker touch process yet, which in this case, we hope
				// that it will in its next restart, and for now, we don't need to do anything. However,
				// if for the currently running WireGuard device that hasn't integrated the up file touching
				// step in its hook since its startup yet, if its appropriate up file is created externally,
				// e.g., by a human operator manually, we can still collect peers data as usual assuming the
				// first tick after the up file creation is a reset tick, and next ticks are diff ticks.
				// It works by making two assumptions:
				// 1) It is the first time that wireuse is running on the machine. Otherwise, labeling the
				//    first tick after manual up file creation as reset tick, might cause incorrect data being
				//    ingested while there might already be some records also labeled as post-reset records
				//    ingested by previous wireuse runs.
				// 2) We hope that the up file touching step is added to the WireGuard device config file's PostUp
				//    hook, and it will be correctly done in its next restart. If not, peers usage data collected
				//    and ingested will be correct only until te current wireuse run.
				log.Warn().Msg("skipping the tick as as wg up file marker does not exist")
				continue
			}

			// TODO: Not always we need to load last peers usage from database, as sometimes it makes no changes to mem.
			// Either we will succeed or fail, mem must not have anything from previous tick.
			mem = make(LastPeersUsage)
			if err := loadLastUsage(ctx, db, f.ModTime().Unix()); nil != err {
				if !errors.Is(err, sql.ErrNoRows) {
					// We can continue WireGuard device ingestion when there are no records already exists.
					// Any other error types, prevents continuing the tick, and as we are sure that we haven't
					// done any changes to the data in the database yet, we log the error hoping that the issue
					// is resolved in the next tick, e.g., by manual intervention of the administrator.
					log.Error().Err(err).Msg("unable to load peers last usage")
					continue
				}
			}

			devName := wgDeviceName(wgUpMarkerFilePath)
			dev, err := wg.Device(devName)
			if nil != err {
				if errors.Is(err, os.ErrNotExist) {
					// It's fine to ignore this error, and wait for another tick as it might occur due to wg device
					// being down.
					log.Info().Str("device name", devName).Msg("discarding this tick as wg device was not found")
					continue
				}
				return fmt.Errorf("unable to get wireguard device '%s' info: %v", devName, err)
			}
			peers := dev.Peers
			peersNo := len(peers)
			if peersNo == 0 {
				log.Info().Msg("skipping the tick as no peers found in wg device")
				continue
			}
			log.
				Debug().
				Func(func(e *zerolog.Event) {
					e.Strs("public keys", funcutils.Map(peers, func(p wgtypes.Peer) string { return p.PublicKey.String() }))
				}).
				Int("number of peers", peersNo).
				Str("wg device name", devName).
				Msg("retrieved wg device peers info")
			rowValues := make([]RowValue, 0, peersNo)
			for _, p := range peers {
				peerPubKey, peerDownload, peerUpload := p.PublicKey.String(), p.TransmitBytes, p.ReceiveBytes
				peerDownload += mem[peerPubKey].Download
				peerUpload += mem[peerPubKey].Upload

				rowValues = append(rowValues, RowValue{
					Peer:     peerPubKey,
					Upload:   peerUpload,
					Download: peerDownload,
					TS:       tickTime,
				})
			}
			query, err := insertQuery(rowValues)
			if nil != err {
				return fmt.Errorf("unable to build insert query")
			}
			log.Debug().Str("query", query).Msg("executing insert query")
			res, err := db.ExecContext(ctx, query)
			if nil != err {
				// As the execution happened in a transaction, we can assume that no partial changes were done,
				// and no existing rows are corrupted. However, we don't want to exit the loop, because if we can
				// successfully ingest data in the next tick, losing one tick has no harm to the correctness of data.
				log.Error().Err(err).Msg("unable to execute all insert query")
				return nil
			}
			expectedAffectedRowsNo := len(rowValues)
			if n, err := res.RowsAffected(); nil != err {
				return fmt.Errorf("unable to get number of affected rows: %v", err)
			} else if n != int64(expectedAffectedRowsNo) {
				return fmt.Errorf("expected %d number of row(s) to be inserted. got %d", expectedAffectedRowsNo, n)
			}
		}
	}
}

type RowValue struct {
	Peer                 string
	Upload, Download, TS int64
}

func newInsertQueryTemplate() func(rowValues []RowValue) (string, error) {
	funcs := template.FuncMap{"join": strings.Join}
	tmpl := template.Must(
		template.
			New("insert row").
			Funcs(funcs).
			Parse(`INSERT INTO peers_usage (peer, upload, download, ts) VALUES {{range $i, $v := .}}{{if $i}}, {{end}}('{{$v.Peer}}', {{$v.Upload}}, {{$v.Download}}, {{$v.TS}}){{end}};`),
	)

	return func(rowValues []RowValue) (string, error) {
		var buf strings.Builder
		if err := tmpl.Execute(&buf, rowValues); nil != err {
			return "", err
		}
		return buf.String(), nil
	}
}

func loadLastUsage(ctx context.Context, db *sql.DB, lastRestartTime int64) error {
	rows, err := db.QueryContext(ctx, fmt.Sprintf("SELECT peer, upload, download, MAX(ts) FROM peers_usage WHERE ts < %d GROUP BY peer;", lastRestartTime))
	if nil != err {
		return fmt.Errorf("unable to query last peers usage: %v", err)
	}
	defer func() {
		if err := rows.Close(); nil != err {
			log.Error().Err(err).Msg("unable to close rows")
		}
	}()

	for rows.Next() {
		var lpu LastPeerUsage
		if err := rows.Scan(&lpu.Peer, &lpu.Upload, &lpu.Download, &lpu.TS); nil != err {
			return fmt.Errorf("unable to scan last peer usage row: %v", err)
		}
		mem[lpu.Peer] = lpu
	}

	if err := rows.Err(); nil != err {
		// Clear mem no matter which error kind it is. It might happen
		// that mem is partially set with values in the previous loop,
		// and that's what we don't want!
		mem = make(LastPeersUsage)
		return err
	}

	return nil
}

func wgDeviceName(wgUpMarkerFilePath string) string {
	return strings.TrimRight(filepath.Base(wgUpMarkerFilePath), ".up")
}
