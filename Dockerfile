FROM docker.io/library/golang:1-alpine AS build

ENV CGO_ENABLED=1

RUN apk update --no-cache && apk upgrade && apk add gcc musl-dev

WORKDIR /workspace

COPY go.sum go.mod ./

ENV GOPROXY=https://goproxy.io,direct

RUN go mod download -x

COPY . .

RUN make build

RUN set -ex && \
    wget https://github.com/upx/upx/releases/download/v4.0.2/upx-4.0.2-amd64_linux.tar.xz && \
    tar -xvf upx-4.0.2-amd64_linux.tar.xz upx-4.0.2-amd64_linux/upx && \
    mv ./upx-4.0.2-amd64_linux/upx . && \
    ./upx --no-color --mono --no-progress --ultra-brute --no-backup ./bin/wireuse && \
    ./upx --test ./bin/wireuse

FROM scratch

COPY --from=build --chown=nonroot:nonroot /workspace/bin/wireuse wireuse

ENTRYPOINT [ "./wireuse" ]